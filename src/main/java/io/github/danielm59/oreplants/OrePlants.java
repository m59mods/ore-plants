package io.github.danielm59.oreplants;

import io.github.danielm59.oreplants.handler.GuiHandler;
import io.github.danielm59.oreplants.proxy.IProxy;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import io.github.danielm59.oreplants.world.WorldGenEssenceOre;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = Reference.MODID, name = Reference.MODNAME, version = Reference.VERSION)
public class OrePlants {

	static {
		FluidRegistry.enableUniversalBucket();
	}

	@Mod.Instance(Reference.MODID)
	public static OrePlants instance;

	@SidedProxy(clientSide = Reference.CPROXY, serverSide = Reference.SPROXY)
	public static IProxy proxy;

	@EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		Registry.fluids();
		proxy.registerModels();
		GameRegistry.registerWorldGenerator(new WorldGenEssenceOre(), 0);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		Registry.crops();
		Registry.items();
		Registry.blocks();
		Registry.recipes();
		proxy.registerTextures();
		proxy.registerTileEntities();
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
	}
}
