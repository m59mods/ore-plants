package io.github.danielm59.oreplants.item;

import io.github.danielm59.oreplants.reference.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemMaterial extends Item {

	public ItemMaterial(String Name) {
		super();
		this.setRegistryName(Reference.MODID, Name.toLowerCase());
		this.setUnlocalizedName(this.getRegistryName().toString());
		GameRegistry.register(this);
		this.setCreativeTab(CreativeTabs.MATERIALS);
	}
	
	public void registerTexture(){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(this, 0,
				new ModelResourceLocation(this.getRegistryName().toString(), "inventory"));
	}

}
