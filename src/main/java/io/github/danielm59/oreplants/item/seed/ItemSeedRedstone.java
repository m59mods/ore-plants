package io.github.danielm59.oreplants.item.seed;

import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemSeedRedstone extends ItemSeedBase {

	static BlockCrops blockCrop = Registry.RedstoneCrop;
	static Block blockSoil = Blocks.FARMLAND;

	public ItemSeedRedstone() {
		super(blockCrop, blockSoil);
		this.setRegistryName(Reference.MODID, "redstoneseeds");
		this.setUnlocalizedName(this.getRegistryName().toString());
		GameRegistry.register(this);
	}
}
