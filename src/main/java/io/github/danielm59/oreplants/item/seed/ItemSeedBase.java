package io.github.danielm59.oreplants.item.seed;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemSeeds;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class ItemSeedBase extends ItemSeeds implements IPlantable
{

    public Block crop;

    public ItemSeedBase(Block blockCrop, Block blockSoil)
    {

	super(blockCrop, blockSoil);
	crop = blockCrop;
	this.setCreativeTab(CreativeTabs.MATERIALS);

    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, BlockPos p)
    {

	return EnumPlantType.Crop;
    }

    @Override
    public IBlockState getPlant(IBlockAccess world, BlockPos p)
    {

	return crop.getDefaultState();
    }

    public void registerTexture()
    {
	Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(this, 0, new ModelResourceLocation(this.getRegistryName().toString(), "inventory"));
    }
}
