package io.github.danielm59.oreplants.item.seed;

import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemSeedObsidian extends ItemSeedBase {

	static BlockCrops blockCrop = Registry.ObsidianCrop;
	static Block blockSoil = Blocks.FARMLAND;

	public ItemSeedObsidian() {
		super(blockCrop, blockSoil);
		this.setRegistryName(Reference.MODID, "obsidianseeds");
		this.setUnlocalizedName(this.getRegistryName().toString());
		GameRegistry.register(this);
	}
}
