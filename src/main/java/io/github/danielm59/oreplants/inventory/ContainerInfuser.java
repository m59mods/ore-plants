package io.github.danielm59.oreplants.inventory;

import io.github.danielm59.oreplants.inventory.slots.SlotOutput;
import io.github.danielm59.oreplants.registry.Registry;
import io.github.danielm59.oreplants.tile.TileInfuser;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerInfuser extends ContainerBase {

	public static final int SLOTS = 5;

	private FluidTank[] fluids = new FluidTank[2];
	private Fluid[] fluidTypes = { FluidRegistry.WATER, Registry.PureEssence };

	private int lastProcessTime;

	private TileInfuser tileInfuser;

	private int lastProcess;

	public ContainerInfuser(InventoryPlayer inventory, TileInfuser tileInfuser, EntityPlayer player) {

		this.tileInfuser = tileInfuser;
		tileInfuser.openInventory(player);

		for (int i = 0; i < fluids.length; ++i) {
			fluids[i] = new FluidTank(this.tileInfuser.getFluidTank(i).getCapacity());
		}

		int slotindex = 0;

		// Add the Input slots to the container
		// Dust input
		this.addSlotToContainer(new Slot(tileInfuser, slotindex++, 80, 35));
		// Water Bucket input
		this.addSlotToContainer(new Slot(tileInfuser, slotindex++, 9, 16));
		// Empty Bucket input
		this.addSlotToContainer(new Slot(tileInfuser, slotindex++, 151, 16));
		// Empty Bucket Output
		this.addSlotToContainer(new SlotOutput(tileInfuser, slotindex++, 9, 54));
		// Essence Bucket Output
		this.addSlotToContainer(new SlotOutput(tileInfuser, slotindex++, 151, 54));

		// Add the player's inventory slots to the container
		for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex) {

			for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex) {

				this.addSlotToContainer(
						new Slot((IInventory) inventory, inventoryColumnIndex + inventoryRowIndex * 9 + 9,
								8 + inventoryColumnIndex * 18, 84 + inventoryRowIndex * 18));

			}

		}

		// Add the player's hot bar slots to the container
		for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex) {

			this.addSlotToContainer(
					new Slot((IInventory) inventory, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));

		}

	}

	@Override
	public void onContainerClosed(EntityPlayer entityPlayer) {

		super.onContainerClosed(entityPlayer);
		tileInfuser.closeInventory(entityPlayer);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex) {

		ItemStack newItemStack = null;
		Slot slot = (Slot) inventorySlots.get(slotIndex);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemStack = slot.getStack();
			newItemStack = itemStack.copy();

			if (slotIndex < SLOTS) {
				if (!this.mergeItemStack(itemStack, SLOTS, inventorySlots.size(), false)) {
					return null;
				}
			} else if (!this.mergeItemStack(itemStack, 0, SLOTS, false)) {
				return null;
			}

			if (itemStack.stackSize == 0) {
				slot.putStack(null);
			} else {
				slot.onSlotChanged();
			}
		}

		return newItemStack;
	}

	public IFluidTank getFluidTank(int i) {
		return fluids[i];
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (IContainerListener crafter : this.listeners) {
			for (int i = 0; i < fluids.length; ++i) {
				if (fluids[i].getFluid() == null) {
					if (tileInfuser.getFluidTank(i).getFluid() != null) {
						if (tileInfuser.getFluidTank(i).getFluidAmount() != 0) {
							fluids[i].setFluid(new FluidStack(tileInfuser.getFluidTank(i).getFluid(),
									tileInfuser.getFluidTank(i).getFluidAmount()));
						} else {
							fluids[i].drain(8000, true);
						}
						crafter.sendProgressBarUpdate(this, i, fluids[i].getFluidAmount());
					}
				} else if (!(fluids[i].getFluid().isFluidEqual(tileInfuser.getFluidTank(i).getFluid()))
						|| fluids[i].getFluidAmount() != tileInfuser.getFluidTank(i).getFluidAmount()) {
					if (tileInfuser.getFluidTank(i).getFluidAmount() != 0) {
						fluids[i].setFluid(new FluidStack(tileInfuser.getFluidTank(i).getFluid(),
								tileInfuser.getFluidTank(i).getFluidAmount()));
					} else {
						fluids[i].drain(8000, true);
					}
					crafter.sendProgressBarUpdate(this, i, fluids[i].getFluidAmount());
				}
			}

			if (this.lastProcessTime != this.tileInfuser.currentProcessTime) {
				crafter.sendProgressBarUpdate(this, fluids.length, this.tileInfuser.currentProcessTime);
			}

		}

		this.lastProcessTime = this.tileInfuser.currentProcessTime;
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int valueType, int updatedValue) {

		if (valueType < fluids.length) {
			this.tileInfuser.getFluidTank(valueType).drain(8000, true);
			this.tileInfuser.getFluidTank(valueType).fill(new FluidStack(fluidTypes[valueType], updatedValue), true);
		}else if (valueType == fluids.length) {
			this.tileInfuser.currentProcessTime = updatedValue;
		}

	}
}
