package io.github.danielm59.oreplants.inventory;

import io.github.danielm59.oreplants.inventory.slots.SlotOutput;
import io.github.danielm59.oreplants.registry.Registry;
import io.github.danielm59.oreplants.tile.TileEnricher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerEnricher extends ContainerBase {

	private TileEnricher tileEnricher;

	public static final int SLOTS = 5;
	
	private int lastProcessTime;

	private FluidTank[] fluids = new FluidTank[1];
	private Fluid[] fluidTypes = { Registry.PureEssence };

	public ContainerEnricher(InventoryPlayer inventory, TileEnricher tileEnricher, EntityPlayer player) {
		this.tileEnricher = tileEnricher;
		tileEnricher.openInventory(player);

		for (int i = 0; i < fluids.length; ++i) {
			fluids[i] = new FluidTank(this.tileEnricher.getFluidTank(i).getCapacity());
		}

		int slotindex = 0;

		// Add the Input slots to the container
		// Essence Bucket input
		this.addSlotToContainer(new Slot(tileEnricher, slotindex++, 20, 16));
		// Empty Bucket output
		this.addSlotToContainer(new SlotOutput(tileEnricher, slotindex++, 20, 54));
		// Seed input
		this.addSlotToContainer(new Slot(tileEnricher, slotindex++, 91, 26));
		// Material input
		this.addSlotToContainer(new Slot(tileEnricher, slotindex++, 91, 44));
		// Seed Output
		this.addSlotToContainer(new SlotOutput(tileEnricher, slotindex++, 139, 35));

		for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex) {

			for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex) {

				this.addSlotToContainer(
						new Slot((IInventory) inventory, inventoryColumnIndex + inventoryRowIndex * 9 + 9,
								8 + inventoryColumnIndex * 18, 84 + inventoryRowIndex * 18));
			}

		}

		// Add the player's hot bar slots to the container
		for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex) {

			this.addSlotToContainer(
					new Slot((IInventory) inventory, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));
		}

	}

	@Override
	public void onContainerClosed(EntityPlayer entityPlayer) {

		super.onContainerClosed(entityPlayer);
		tileEnricher.closeInventory(entityPlayer);
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex) {

		ItemStack newItemStack = null;
		Slot slot = (Slot) inventorySlots.get(slotIndex);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemStack = slot.getStack();
			newItemStack = itemStack.copy();

			if (slotIndex < SLOTS) {
				if (!this.mergeItemStack(itemStack, SLOTS, inventorySlots.size(), false)) {
					return null;
				}
			} else if (!this.mergeItemStack(itemStack, 0, SLOTS, false)) {
				return null;
			}

			if (itemStack.stackSize == 0) {
				slot.putStack(null);
			} else {
				slot.onSlotChanged();
			}
		}

		return newItemStack;
	}

	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (IContainerListener crafter : this.listeners) {
			for (int i = 0; i < fluids.length; ++i) {
				if (fluids[i].getFluid() == null) {
					if (tileEnricher.getFluidTank(i).getFluid() != null) {
						if (tileEnricher.getFluidTank(i).getFluidAmount() != 0) {
							fluids[i].setFluid(new FluidStack(tileEnricher.getFluidTank(i).getFluid(),
									tileEnricher.getFluidTank(i).getFluidAmount()));
						} else {
							fluids[i].drain(8000, true);
						}
						crafter.sendProgressBarUpdate(this, i, fluids[i].getFluidAmount());
					}
				} else if (!(fluids[i].getFluid().isFluidEqual(tileEnricher.getFluidTank(i).getFluid()))
						|| fluids[i].getFluidAmount() != tileEnricher.getFluidTank(i).getFluidAmount()) {
					if (tileEnricher.getFluidTank(i).getFluidAmount() != 0) {
						fluids[i].setFluid(new FluidStack(tileEnricher.getFluidTank(i).getFluid(),
								tileEnricher.getFluidTank(i).getFluidAmount()));
					} else {
						fluids[i].drain(8000, true);
					}
					crafter.sendProgressBarUpdate(this, i, fluids[i].getFluidAmount());
				}
			}

			 if (this.lastProcessTime != this.tileEnricher.currentProcessTime)
			 {
			 crafter.sendProgressBarUpdate(this, fluids.length,
			 this.tileEnricher.currentProcessTime);
			 }

		}

		this.lastProcessTime = this.tileEnricher.currentProcessTime;
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int valueType, int updatedValue) {

		if (valueType < fluids.length) {
			this.tileEnricher.getFluidTank(valueType).drain(8000, true);
			this.tileEnricher.getFluidTank(valueType).fill(new FluidStack(fluidTypes[valueType], updatedValue), true);
			 }else if (valueType == fluids.length) {
			 this.tileEnricher.currentProcessTime = updatedValue;
		}

	}

}
