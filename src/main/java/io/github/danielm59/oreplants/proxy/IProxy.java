package io.github.danielm59.oreplants.proxy;

public interface IProxy {

	public void registerModels();

	public void registerTileEntities();

	public void registerTextures();

}
