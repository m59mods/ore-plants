package io.github.danielm59.oreplants.proxy;

import io.github.danielm59.oreplants.tile.TileEnricher;
import io.github.danielm59.oreplants.tile.TileInfuser;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ServerProxy implements IProxy {

	@Override
	public void registerModels() {

	}

	@Override
	public void registerTileEntities() {
		GameRegistry.registerTileEntityWithAlternatives(TileInfuser.class, "infuser", "tile." + "infuser");
		GameRegistry.registerTileEntityWithAlternatives(TileEnricher.class, "enricher", "tile." + "enricher");
	}

	@Override
	public void registerTextures() {
		
	}

}
