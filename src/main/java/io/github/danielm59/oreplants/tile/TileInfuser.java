package io.github.danielm59.oreplants.tile;

import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.UniversalBucket;

public class TileInfuser extends TileBase implements ITickable {

	private final int RecipeAmount = 100;
	public final int ProcessTime = 100;

	public int currentProcessTime = 0;

	public TileInfuser() {
		super();
		inventory = new ItemStack[5];
		fluids = new FluidTank[2];
		initTanks(8000);
	}

	@Override
	public String getName() {
		return "Infuser";
	}

	@Override
	public void update() {
		if (!worldObj.isRemote) {
			FillWater();
			Process();
			EmptyEssence();
		}
	}

	private void FillWater() {
		if (getStackInSlot(1) != null) {
			if (getStackInSlot(1).isItemEqual(new ItemStack(Items.WATER_BUCKET))) {
				if (getStackInSlot(3) == null || (getStackInSlot(3).isItemEqual(new ItemStack(Items.BUCKET))
						& getStackInSlot(3).stackSize != getStackInSlot(3).getItem()
								.getItemStackLimit(getStackInSlot(3)))) {
					if (getFluidTank(0).getCapacity() - getFluidTank(0).getFluidAmount() >= Fluid.BUCKET_VOLUME) {
						this.markDirty();
						FluidStack water = new FluidStack(FluidRegistry.WATER, Fluid.BUCKET_VOLUME);
						getFluidTank(0).fill(water, true);
						if (getStackInSlot(3) == null) {
							setInventorySlotContents(3, new ItemStack(Items.BUCKET));
						} else {
							ItemStack stack = getStackInSlot(3);
							stack.stackSize++;
							setInventorySlotContents(3, stack);
						}
						decrStackSize(1, 1);
					}
				}
			}
		}
	}

	private void Process() {
		if (getStackInSlot(0) != null) {
			if ((getStackInSlot(0).getItem() == Registry.EssenceDust) && fluids[0].getFluidAmount() >= RecipeAmount
					&& (getFluidTank(1).getCapacity() - getFluidTank(1).getFluidAmount() >= RecipeAmount)) {
				if (++currentProcessTime >= ProcessTime) {
					this.markDirty();
					currentProcessTime = 0;
					FluidStack essence = new FluidStack(Registry.PureEssence, RecipeAmount);
					getFluidTank(1).fill(essence, true);
					FluidStack water = new FluidStack(FluidRegistry.WATER, RecipeAmount);
					getFluidTank(0).drain(water, true);
					decrStackSize(0, 1);
				}
			}
		} else {
			currentProcessTime = 0;
		}
	}

	private void EmptyEssence() {
		if (getStackInSlot(2) != null && getStackInSlot(4) == null) {
			if((fluids[1].getFluidAmount()>= Fluid.BUCKET_VOLUME) && getStackInSlot(2).getItem()==Items.BUCKET){
				this.markDirty();
				decrStackSize(2, 1);
				FluidStack essence = new FluidStack(Registry.PureEssence, Fluid.BUCKET_VOLUME);
				setInventorySlotContents(4, UniversalBucket.getFilledBucket(ForgeModContainer.getInstance().universalBucket, essence.getFluid()));
				getFluidTank(1).drain(essence, true);
			}
		}
	}
}
