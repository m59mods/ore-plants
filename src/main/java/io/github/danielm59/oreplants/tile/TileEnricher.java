package io.github.danielm59.oreplants.tile;

import io.github.danielm59.oreplants.recipe.EnricherRecipe;
import io.github.danielm59.oreplants.recipe.EnricherRegistry;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.UniversalBucket;

public class TileEnricher extends TileBase implements ITickable {

	private final int RecipeAmount = 1000;
	public final int ProcessTime = 100;

	public int currentProcessTime = 0;
	
	private EnricherRecipe lastRecipe;

	public TileEnricher() {
		super();
		inventory = new ItemStack[5];
		fluids = new FluidTank[1];
		initTanks(8000);
	}

	@Override
	public String getName() {
		return "Enricher";
	}

	@Override
	public void update() {
		if (!worldObj.isRemote) {
			FillEssence();
			Process();
		}
	}

	private void FillEssence() {
		if (getStackInSlot(0) != null) {
			if (getStackInSlot(0).isItemEqual(UniversalBucket
					.getFilledBucket(ForgeModContainer.getInstance().universalBucket, Registry.PureEssence))) {
				if (getStackInSlot(1) == null || (getStackInSlot(1).isItemEqual(new ItemStack(Items.BUCKET))
						& getStackInSlot(1).stackSize != getStackInSlot(1).getItem()
								.getItemStackLimit(getStackInSlot(1)))) {
					if (getFluidTank(0).getCapacity() - getFluidTank(0).getFluidAmount() >= Fluid.BUCKET_VOLUME) {
						this.markDirty();
						FluidStack essence = new FluidStack(Registry.PureEssence, Fluid.BUCKET_VOLUME);
						getFluidTank(0).fill(essence, true);
						if (getStackInSlot(1) == null) {
							setInventorySlotContents(1, new ItemStack(Items.BUCKET));
						} else {
							ItemStack stack = getStackInSlot(1);
							stack.stackSize++;
							setInventorySlotContents(1, stack);
						}
						decrStackSize(0, 1);
					}
				}
			}
		}
	}

	public void Process() {
		EnricherRecipe recipe = EnricherRegistry.getInstance().getMatchingRecipe(inventory[2], inventory[3],
				inventory[4]);
		if (recipe != lastRecipe){
			currentProcessTime = 0;
			lastRecipe = recipe;
		}
		if (recipe != null) {
			if (fluids[0].getFluidAmount() >= RecipeAmount) {
				this.markDirty();
				if (++currentProcessTime >= 100) {
					this.markDirty();
					currentProcessTime = 0;
					if (inventory[4] != null) {
						inventory[4].stackSize += recipe.getOutput().stackSize;
					} else {
						inventory[4] = recipe.getOutput().copy();
					}
					if (inventory[2].getItem().hasContainerItem(inventory[2])) {
						setInventorySlotContents(2, inventory[2].getItem().getContainerItem(inventory[2]));
					} else {
						decrStackSize(2, recipe.getInputTop().stackSize);
					}
					if (inventory[3].getItem().hasContainerItem(inventory[3])) {
						setInventorySlotContents(3, inventory[3].getItem().getContainerItem(inventory[3]));
					} else {
						decrStackSize(3, recipe.getInputBottom().stackSize);
					}
					fluids[0].drain(RecipeAmount, true);
				}
			}
		}
	}

}
