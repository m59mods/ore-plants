package io.github.danielm59.oreplants.registry;

import io.github.danielm59.oreplants.block.BlockEnricher;
import io.github.danielm59.oreplants.block.BlockEssence;
import io.github.danielm59.oreplants.block.BlockEssenceOre;
import io.github.danielm59.oreplants.block.BlockInfuser;
import io.github.danielm59.oreplants.block.crop.BlockCropBase;
import io.github.danielm59.oreplants.block.crop.BlockCropCoal;
import io.github.danielm59.oreplants.block.crop.BlockCropDiamond;
import io.github.danielm59.oreplants.block.crop.BlockCropEmerald;
import io.github.danielm59.oreplants.block.crop.BlockCropGold;
import io.github.danielm59.oreplants.block.crop.BlockCropIron;
import io.github.danielm59.oreplants.block.crop.BlockCropLapis;
import io.github.danielm59.oreplants.block.crop.BlockCropNether;
import io.github.danielm59.oreplants.block.crop.BlockCropObsidian;
import io.github.danielm59.oreplants.block.crop.BlockCropQuartz;
import io.github.danielm59.oreplants.block.crop.BlockCropRedstone;
import io.github.danielm59.oreplants.fluid.FluidEssence;
import io.github.danielm59.oreplants.item.ItemMaterial;
import io.github.danielm59.oreplants.item.seed.ItemSeedBase;
import io.github.danielm59.oreplants.item.seed.ItemSeedCoal;
import io.github.danielm59.oreplants.item.seed.ItemSeedDiamond;
import io.github.danielm59.oreplants.item.seed.ItemSeedEmerald;
import io.github.danielm59.oreplants.item.seed.ItemSeedGold;
import io.github.danielm59.oreplants.item.seed.ItemSeedIron;
import io.github.danielm59.oreplants.item.seed.ItemSeedLapis;
import io.github.danielm59.oreplants.item.seed.ItemSeedNether;
import io.github.danielm59.oreplants.item.seed.ItemSeedObsidian;
import io.github.danielm59.oreplants.item.seed.ItemSeedQuartz;
import io.github.danielm59.oreplants.item.seed.ItemSeedRedstone;
import io.github.danielm59.oreplants.recipe.EnricherRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class Registry {

	public static ItemMaterial EssenceDust;

	public static BlockCropBase CoalCrop;
	public static ItemSeedBase CoalSeeds;
	public static ItemMaterial CoalShard;

	public static BlockCropBase DiamondCrop;
	public static ItemSeedBase DiamondSeeds;
	public static ItemMaterial DiamondShard;

	public static BlockCropBase EmeraldCrop;
	public static ItemSeedBase EmeraldSeeds;
	public static ItemMaterial EmeraldShard;

	public static BlockCropBase GoldCrop;
	public static ItemSeedBase GoldSeeds;
	public static ItemMaterial GoldShard;

	public static BlockCropBase IronCrop;
	public static ItemSeedBase IronSeeds;
	public static ItemMaterial IronShard;

	public static BlockCropBase LapisCrop;
	public static ItemSeedBase LapisSeeds;
	public static ItemMaterial LapisShard;

	public static BlockCropBase NetherCrop;
	public static ItemSeedBase NetherSeeds;
	public static ItemMaterial NetherShard;

	public static BlockCropBase ObsidianCrop;
	public static ItemSeedBase ObsidianSeeds;
	public static ItemMaterial ObsidianShard;

	public static BlockCropBase QuartzCrop;
	public static ItemSeedBase QuartzSeeds;
	public static ItemMaterial QuartzShard;

	public static BlockCropBase RedstoneCrop;
	public static ItemSeedBase RedstoneSeeds;
	public static ItemMaterial RedstoneShard;

	public static FluidEssence PureEssence;

	public static BlockEssenceOre EssenceOre;
	public static BlockEssence EssenceBlock;

	public static BlockInfuser Infuser;
	public static BlockEnricher Enricher;

	public static void crops() {
		CoalCrop = new BlockCropCoal();
		DiamondCrop = new BlockCropDiamond();
		EmeraldCrop = new BlockCropEmerald();
		GoldCrop = new BlockCropGold();
		IronCrop = new BlockCropIron();
		LapisCrop = new BlockCropLapis();
		NetherCrop = new BlockCropNether();
		ObsidianCrop = new BlockCropObsidian();
		QuartzCrop = new BlockCropQuartz();
		RedstoneCrop = new BlockCropRedstone();

		CoalSeeds = new ItemSeedCoal();
		DiamondSeeds = new ItemSeedDiamond();
		EmeraldSeeds = new ItemSeedEmerald();
		GoldSeeds = new ItemSeedGold();
		IronSeeds = new ItemSeedIron();
		LapisSeeds = new ItemSeedLapis();
		NetherSeeds = new ItemSeedNether();
		ObsidianSeeds = new ItemSeedObsidian();
		QuartzSeeds = new ItemSeedQuartz();
		RedstoneSeeds = new ItemSeedRedstone();
	}

	public static void items() {

		EssenceDust = new ItemMaterial("essencedust");

		CoalShard = new ItemMaterial("coalshard");
		DiamondShard = new ItemMaterial("diamondshard");
		EmeraldShard = new ItemMaterial("emeraldshard");
		GoldShard = new ItemMaterial("goldshard");
		IronShard = new ItemMaterial("ironshard");
		LapisShard = new ItemMaterial("lapisshard");
		NetherShard = new ItemMaterial("nethershard");
		ObsidianShard = new ItemMaterial("obsidianshard");
		QuartzShard = new ItemMaterial("quartzshard");
		RedstoneShard = new ItemMaterial("redstoneshard");
	}

	public static void blocks() {

		EssenceOre = new BlockEssenceOre();
		EssenceBlock = new BlockEssence();
		Infuser = new BlockInfuser();
		Enricher = new BlockEnricher();

	}

	public static void recipes() {
		GameRegistry.addRecipe(new ShapedOreRecipe(EssenceBlock, "ddd", "ddd", "ddd", 'd', EssenceDust));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(EssenceDust, 9), EssenceBlock));

		GameRegistry.addRecipe(new ShapedOreRecipe(Infuser, "iii", "bgb", "iii", 'i', "ingotIron", 'b', Items.BUCKET, 'g', Blocks.GLASS_PANE));
		GameRegistry.addRecipe(new ShapedOreRecipe(Enricher, "i i", "ibi", "iii", 'i', "ingotIron", 'b', Items.BUCKET));
		
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.COAL, "sss", "sss", "sss", 's', CoalShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.DIAMOND, "sss", "sss", "sss", 's', DiamondShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.EMERALD, "sss", "sss", "sss", 's', EmeraldShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.GOLD_INGOT, "sss", "sss", "sss", 's', GoldShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.IRON_INGOT, "sss", "sss", "sss", 's', IronShard));
		GameRegistry
				.addRecipe(new ShapedOreRecipe(new ItemStack(Items.DYE, 1, 4), "sss", "sss", "sss", 's', LapisShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Blocks.NETHERRACK, "sss", "sss", "sss", 's', NetherShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Blocks.OBSIDIAN, "sss", "sss", "sss", 's', ObsidianShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.QUARTZ, "sss", "sss", "sss", 's', QuartzShard));
		GameRegistry.addRecipe(new ShapedOreRecipe(Items.REDSTONE, "sss", "sss", "sss", 's', RedstoneShard));

		EnricherRegistry.addRecipe(new ItemStack(Blocks.COAL_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(CoalSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.DIAMOND_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(DiamondSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.EMERALD_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(EmeraldSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.GOLD_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(GoldSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.IRON_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(IronSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.LAPIS_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(LapisSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.NETHER_BRICK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(NetherSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.OBSIDIAN), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(ObsidianSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.QUARTZ_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(QuartzSeeds));
		EnricherRegistry.addRecipe(new ItemStack(Blocks.REDSTONE_BLOCK), new ItemStack(Items.WHEAT_SEEDS),
				new ItemStack(RedstoneSeeds));
	}

	public static void fluids() {
		PureEssence = new FluidEssence("pure", 0xFFFFFF);
	}

	public static void registerTextures() {
		EssenceDust.registerTexture();
		CoalSeeds.registerTexture();
		DiamondSeeds.registerTexture();
		EmeraldSeeds.registerTexture();
		GoldSeeds.registerTexture();
		IronSeeds.registerTexture();
		LapisSeeds.registerTexture();
		NetherSeeds.registerTexture();
		ObsidianSeeds.registerTexture();
		QuartzSeeds.registerTexture();
		RedstoneSeeds.registerTexture();
		CoalShard.registerTexture();
		DiamondShard.registerTexture();
		EmeraldShard.registerTexture();
		GoldShard.registerTexture();
		IronShard.registerTexture();
		LapisShard.registerTexture();
		NetherShard.registerTexture();
		ObsidianShard.registerTexture();
		QuartzShard.registerTexture();
		RedstoneShard.registerTexture();
		EssenceOre.registerTexture();
		EssenceBlock.registerTexture();
		Infuser.registerTexture();
		Enricher.registerTexture();
	}

}
