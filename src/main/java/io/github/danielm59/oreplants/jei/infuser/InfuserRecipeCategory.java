package io.github.danielm59.oreplants.jei.infuser;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeCategory;
import io.github.danielm59.oreplants.jei.RecipeCategoryUid;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiFluidStackGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidRegistry;

public class InfuserRecipeCategory extends RecipeCategory {

	private static final int inputSlot = 0;
	
	private static final int inputTank = 0;
	private static final int outputTank = 1;

	private final static ResourceLocation guiTexture = new ResourceLocation(Reference.MODID.toLowerCase(),
			"textures/gui/infuser.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	public InfuserRecipeCategory(IGuiHelper guiHelper) {
		super(guiHelper.createDrawable(guiTexture, 30, 6, 116, 74), Registry.Enricher.getUnlocalizedName() + ".name");

		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 24, 17);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 100, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public String getUid() {
		return RecipeCategoryUid.INFUSER;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft) {
		arrow.draw(minecraft, 21, 29);
		arrow.draw(minecraft, 71, 29);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper) {
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
		
		guiItemStacks.init(inputSlot, true, 49, 28);
		
		guiFluidStacks.init(inputTank, true, 2, 2, 16, 70, 8000, true, null);
		guiFluidStacks.init(outputTank, false,98, 2, 16, 70, 8000, true, null);

		guiItemStacks.setFromRecipe(inputSlot, recipeWrapper.getInputs().get(1));
		
		guiFluidStacks.set(inputTank, FluidRegistry.getFluidStack("water", 100));
		guiFluidStacks.set(outputTank, FluidRegistry.getFluidStack("pure", 100));

	}

}
