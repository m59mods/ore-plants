package io.github.danielm59.oreplants.jei.enricher;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

public class EnricherRecipeHandler implements IRecipeHandler<EnricherRecipeWrapper>
{

	@Nonnull
	@Override
	public Class<EnricherRecipeWrapper> getRecipeClass()
	{
		return EnricherRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return RecipeCategoryUid.ENRICHER;
	}
	
	@Nonnull
	@Override
	public String getRecipeCategoryUid(EnricherRecipeWrapper recipe) {
		return RecipeCategoryUid.ENRICHER;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull EnricherRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(EnricherRecipeWrapper wrapper)
	{
		return true;
	}

}
