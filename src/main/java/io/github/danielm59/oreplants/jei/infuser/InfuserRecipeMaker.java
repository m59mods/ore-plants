package io.github.danielm59.oreplants.jei.infuser;

import java.util.ArrayList;
import java.util.List;

public class InfuserRecipeMaker {
	private InfuserRecipeMaker() {
	}

	public static List<InfuserRecipeWrapper> getRecipes() {
		List<InfuserRecipeWrapper> recipes = new ArrayList<InfuserRecipeWrapper>();
		recipes.add(new InfuserRecipeWrapper());
		return recipes;
	}
}
