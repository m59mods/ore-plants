package io.github.danielm59.oreplants.jei;

import javax.annotation.Nonnull;

import mezz.jei.api.recipe.BlankRecipeWrapper;

public abstract class RecipeWrapper<R> extends BlankRecipeWrapper
{
	@Nonnull
	private final R recipe;

	public RecipeWrapper(@Nonnull R recipe)
	{
		this.recipe = recipe;
	}

	@Nonnull
	public R getRecipe()
	{
		return recipe;
	}
}
