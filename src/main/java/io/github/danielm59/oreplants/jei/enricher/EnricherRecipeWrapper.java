package io.github.danielm59.oreplants.jei.enricher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeWrapper;
import io.github.danielm59.oreplants.recipe.EnricherRecipe;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.fluids.UniversalBucket;

public class EnricherRecipeWrapper extends RecipeWrapper<EnricherRecipe> {

	public EnricherRecipeWrapper(EnricherRecipe recipe) {
		super(recipe);
	}

	@Nonnull
	@Override
	public List<ItemStack> getInputs() {
		List<ItemStack> inputs = new ArrayList<ItemStack>();
		inputs.add(UniversalBucket.getFilledBucket(ForgeModContainer.getInstance().universalBucket, Registry.PureEssence));
		inputs.add(getRecipe().getInputTop());
		inputs.add(getRecipe().getInputBottom());
		return inputs;
	}

	@Nonnull
	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(getRecipe().getOutput());
	}
}
