package io.github.danielm59.oreplants.jei;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.gui.GuiEnricher;
import io.github.danielm59.oreplants.gui.GuiInfuser;
import io.github.danielm59.oreplants.jei.enricher.EnricherRecipeCategory;
import io.github.danielm59.oreplants.jei.enricher.EnricherRecipeHandler;
import io.github.danielm59.oreplants.jei.enricher.EnricherRecipeMaker;
import io.github.danielm59.oreplants.jei.infuser.InfuserRecipeCategory;
import io.github.danielm59.oreplants.jei.infuser.InfuserRecipeHandler;
import io.github.danielm59.oreplants.jei.infuser.InfuserRecipeMaker;
import io.github.danielm59.oreplants.registry.Registry;
import mezz.jei.api.BlankModPlugin;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import net.minecraft.item.ItemStack;

@JEIPlugin
public class JeiPlugin extends BlankModPlugin {
	@Override
	public void register(@Nonnull IModRegistry registry) {
		IJeiHelpers jeiHelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();

		registry.addRecipeCategories(new InfuserRecipeCategory(guiHelper));
		registry.addRecipeCategories(new EnricherRecipeCategory(guiHelper));

		registry.addRecipeHandlers(new InfuserRecipeHandler());
		registry.addRecipeHandlers(new EnricherRecipeHandler());

		registry.addRecipes(InfuserRecipeMaker.getRecipes());
		registry.addRecipes(EnricherRecipeMaker.getRecipes());

		registry.addRecipeClickArea(GuiInfuser.class, 51, 35, 24, 17, RecipeCategoryUid.INFUSER);
		registry.addRecipeClickArea(GuiInfuser.class, 101, 35, 24, 17, RecipeCategoryUid.INFUSER);
		registry.addRecipeClickArea(GuiEnricher.class, 62, 35, 24, 17, RecipeCategoryUid.ENRICHER);
		registry.addRecipeClickArea(GuiEnricher.class, 112, 35, 24, 17, RecipeCategoryUid.ENRICHER);

		registry.addRecipeCategoryCraftingItem(new ItemStack(Registry.Infuser), RecipeCategoryUid.INFUSER);
		registry.addRecipeCategoryCraftingItem(new ItemStack(Registry.Enricher), RecipeCategoryUid.ENRICHER);
	}
}
