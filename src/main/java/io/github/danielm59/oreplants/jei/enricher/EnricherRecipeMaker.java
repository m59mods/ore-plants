package io.github.danielm59.oreplants.jei.enricher;

import java.util.ArrayList;
import java.util.List;

import io.github.danielm59.oreplants.recipe.EnricherRecipe;
import io.github.danielm59.oreplants.recipe.EnricherRegistry;

public class EnricherRecipeMaker {
	private EnricherRecipeMaker() {
	}

	public static List<EnricherRecipeWrapper> getRecipes() {
		List<EnricherRecipeWrapper> recipes = new ArrayList<EnricherRecipeWrapper>();
		for (EnricherRecipe recipe : EnricherRegistry.getAllRecipes())
		{
			recipes.add(new EnricherRecipeWrapper(recipe));
		}
		return recipes;
	}
}
