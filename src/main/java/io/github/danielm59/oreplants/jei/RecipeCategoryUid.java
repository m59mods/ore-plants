package io.github.danielm59.oreplants.jei;

public class RecipeCategoryUid
{

	public static final String INFUSER = "oreplants.infuser";
	public static final String ENRICHER = "oreplants.enricher";

}
