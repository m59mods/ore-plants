package io.github.danielm59.oreplants.jei.infuser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeWrapper;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.ForgeModContainer;
import net.minecraftforge.fluids.UniversalBucket;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class InfuserRecipeWrapper extends RecipeWrapper {

	public InfuserRecipeWrapper() {
		super(null);
	}
	//RECIPE HARD CODED UNTIL I FIND A BETTER WAY TO HANDLE RECIPES WITH FLUIDS
	@Nonnull
	@Override
	public List<ItemStack> getInputs() {
		List<ItemStack> inputs = new ArrayList<ItemStack>();
		;
		inputs.add(new ItemStack(Items.BUCKET));
		inputs.add(new ItemStack(Registry.EssenceDust));
		return inputs;
	}

	@Nonnull
	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(UniversalBucket.getFilledBucket(ForgeModContainer.getInstance().universalBucket, Registry.PureEssence));
	}
}
