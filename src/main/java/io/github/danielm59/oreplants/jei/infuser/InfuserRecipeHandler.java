package io.github.danielm59.oreplants.jei.infuser;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

public class InfuserRecipeHandler implements IRecipeHandler<InfuserRecipeWrapper>
{

	@Nonnull
	@Override
	public Class<InfuserRecipeWrapper> getRecipeClass()
	{
		return InfuserRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return RecipeCategoryUid.INFUSER;
	}
	
	@Nonnull
	@Override
	public String getRecipeCategoryUid(InfuserRecipeWrapper recipe) {
		return RecipeCategoryUid.INFUSER;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull InfuserRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(InfuserRecipeWrapper wrapper)
	{
		return true;
	}

}
