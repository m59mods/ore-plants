package io.github.danielm59.oreplants.jei.enricher;

import javax.annotation.Nonnull;

import io.github.danielm59.oreplants.jei.RecipeCategory;
import io.github.danielm59.oreplants.jei.RecipeCategoryUid;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiFluidStackGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidRegistry;

public class EnricherRecipeCategory extends RecipeCategory {

	private static final int inputSlot = 0;
	private static final int bottomSlot = 1;
	private static final int outputSlot = 2;
	
	private static final int inputTank = 0;

	private final static ResourceLocation guiTexture = new ResourceLocation(Reference.MODID.toLowerCase(),
			"textures/gui/enricher.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	public EnricherRecipeCategory(IGuiHelper guiHelper) {
		super(guiHelper.createDrawable(guiTexture, 41, 6, 116, 74), Registry.Enricher.getUnlocalizedName() + ".name");

		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 24, 17);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 100, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public String getUid() {
		return RecipeCategoryUid.ENRICHER;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft) {
		arrow.draw(minecraft, 21, 29);
		arrow.draw(minecraft, 71, 29);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper) {
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
		
		guiItemStacks.init(inputSlot, true, 49, 19);
		guiItemStacks.init(bottomSlot, true, 49, 37);
		guiItemStacks.init(outputSlot, true, 97, 28);
		
		guiFluidStacks.init(inputTank, true, 2, 2, 16, 70, 8000, true, null);

		guiItemStacks.setFromRecipe(inputSlot, recipeWrapper.getInputs().get(1));
		guiItemStacks.setFromRecipe(bottomSlot, recipeWrapper.getInputs().get(2));
		guiItemStacks.setFromRecipe(outputSlot, recipeWrapper.getOutputs());
		
		guiFluidStacks.set(inputTank, FluidRegistry.getFluidStack("pure", 1000));

	}

}
