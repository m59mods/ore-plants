package io.github.danielm59.oreplants.gui;

import org.lwjgl.opengl.GL11;

import io.github.danielm59.oreplants.gui.util.TankUtil;
import io.github.danielm59.oreplants.inventory.ContainerInfuser;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.tile.TileInfuser;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiInfuser extends GuiContainer {

	private TileInfuser tileInfuser;

	public GuiInfuser(InventoryPlayer inventoryPlayer, TileInfuser infuser, EntityPlayer player) {
		super(new ContainerInfuser(inventoryPlayer, infuser, player));
		tileInfuser = infuser;
		xSize = 176;
		ySize = 166;

	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y) {
		String text = I18n.format(tileInfuser.getName());
		int pos = (xSize - fontRendererObj.getStringWidth(text)) / 2;
		fontRendererObj.drawString(text, pos, 6, 0x404040);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MODID, "textures/gui/infuser.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		float time = 1f
				- ((float) (tileInfuser.ProcessTime - tileInfuser.currentProcessTime) / tileInfuser.ProcessTime);
		if (time > 0f) {
			if (time < 0.5f) {
				this.drawTexturedModalRect(xStart + 51, yStart + 35, 176, 0, (int) (24f * 2f * time), 17);
			} else {
				this.drawTexturedModalRect(xStart + 51, yStart + 35, 176, 0, 24, 17);
				this.drawTexturedModalRect(xStart + 101, yStart + 35, 176, 0, (int) (24f * 2f * (time - 0.5f)), 17);
			}
		}
		TankUtil.draw(32, 8, 70, 16, xStart, yStart, tileInfuser.getFluidTank(0));
		TankUtil.draw(128, 8, 70, 16, xStart, yStart, tileInfuser.getFluidTank(1));
	}
}
