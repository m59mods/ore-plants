package io.github.danielm59.oreplants.gui;

import org.lwjgl.opengl.GL11;

import io.github.danielm59.oreplants.gui.util.TankUtil;
import io.github.danielm59.oreplants.inventory.ContainerEnricher;
import io.github.danielm59.oreplants.inventory.ContainerInfuser;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.tile.TileEnricher;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiEnricher extends GuiContainer {

	private TileEnricher tileEnricher;

	public GuiEnricher(InventoryPlayer inventoryPlayer, TileEnricher enricher, EntityPlayer player) {
		super(new ContainerEnricher(inventoryPlayer, enricher, player));
		tileEnricher = enricher;
		xSize = 176;
		ySize = 166;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y) {
		String text = I18n.format(tileEnricher.getName());
		int pos = (xSize - fontRendererObj.getStringWidth(text)) / 2;
		fontRendererObj.drawString(text, pos, 6, 0x404040);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MODID, "textures/gui/enricher.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		float time = 1f
				- ((float) (tileEnricher.ProcessTime - tileEnricher.currentProcessTime) / tileEnricher.ProcessTime);
		if (time > 0f) {
			if (time < 0.5f) {
				this.drawTexturedModalRect(xStart + 62, yStart + 35, 176, 0, (int) (24f * 2f * time), 17);
			} else {
				this.drawTexturedModalRect(xStart + 62, yStart + 35, 176, 0, 24, 17);
				this.drawTexturedModalRect(xStart + 112, yStart + 35, 176, 0, (int) (24f * 2f * (time - 0.5f)), 17);
			}
		}
		TankUtil.draw(43, 8, 70, 16, xStart, yStart, tileEnricher.getFluidTank(0));	
	}

}
