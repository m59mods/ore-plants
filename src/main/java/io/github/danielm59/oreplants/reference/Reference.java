package io.github.danielm59.oreplants.reference;

public class Reference {

	public static final String MODID = "oreplants";
	public static final String MODNAME = "Ore Plants";
	public static final String VERSION = "@VERSION@";
	public static final String CPROXY = "io.github.danielm59.oreplants.proxy.ClientProxy";
	public static final String SPROXY = "io.github.danielm59.oreplants.proxy.ServerProxy";

}
