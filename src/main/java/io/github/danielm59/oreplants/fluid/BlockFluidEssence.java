package io.github.danielm59.oreplants.fluid;

import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class BlockFluidEssence extends BlockFluidClassic {

	public BlockFluidEssence(Fluid fluid) {
		super(fluid, Material.WATER);
	}

}
