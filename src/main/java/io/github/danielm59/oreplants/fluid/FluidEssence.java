package io.github.danielm59.oreplants.fluid;

import io.github.danielm59.oreplants.reference.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.BlockFluidBase;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class FluidEssence extends Fluid {

	public static ResourceLocation ICON_LiquidStill = new ResourceLocation(Reference.MODID, "blocks/fluids/liquid");
	public static ResourceLocation ICON_LiquidFlowing = new ResourceLocation(Reference.MODID,
			"blocks/fluids/liquid_flow");

	public final int colour;

	public FluidEssence(String fluidName, int colour) {
		super(fluidName, ICON_LiquidStill, ICON_LiquidFlowing);
		if (((colour >> 24) & 0xFF) == 0) {
			colour |= 0xFF << 24;
		}
		this.setLuminosity(12);
		this.colour = colour;
		this.setUnlocalizedName(new ResourceLocation(Reference.MODID, "fluid" + fluidName.toLowerCase()).toString());
		FluidRegistry.registerFluid(this);
		registerBlock(this);
		FluidRegistry.addBucketForFluid(this);

	}

	private void registerBlock(Fluid fluid) {
		BlockFluidBase block = new BlockFluidEssence(fluid);
		block.setRegistryName(this.unlocalizedName);
		GameRegistry.register(block);
	}

	@Override
	public int getColor() {
		return colour;
	}

}
