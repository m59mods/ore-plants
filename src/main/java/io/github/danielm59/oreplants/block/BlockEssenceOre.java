package io.github.danielm59.oreplants.block;

import java.util.Random;

import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockEssenceOre extends Block {

	private final Item item;
	
	public BlockEssenceOre() {
		super(Material.IRON);
		this.setSoundType(SoundType.STONE);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		this.setHardness(2.5F);
		this.setResistance(10.0F);
		this.setRegistryName(new ResourceLocation(Reference.MODID, "essenceore"));
		this.setUnlocalizedName(this.getRegistryName().toString());
		item = new ItemBlock(this);
		item.setRegistryName(this.getRegistryName());
		item.setUnlocalizedName(this.getUnlocalizedName());
		GameRegistry.register(this);
		GameRegistry.register(item);
	}

	@Override
	public int quantityDropped(Random rand) {

		return 2 + rand.nextInt(4);
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune) {

		return Registry.EssenceDust;
	}

	@Override
	protected boolean canSilkHarvest() {

		return true;
	}

	@Override
	public int quantityDroppedWithBonus(int fortune, Random rand) {

		if (fortune > 0 && Item.getItemFromBlock(this) != this.getItemDropped(this.getDefaultState(), rand, fortune)) {
			int j = rand.nextInt(fortune + 2) - 1;
			if (j < 0) {
				j = 0;
			}
			return this.quantityDropped(rand) * (j + 1);
		} else {
			return this.quantityDropped(rand);
		}
	}

	@Override
	public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune) {
		Random random = new Random();
		return 3 + random.nextInt(2);
	}
	
	public void registerTexture(){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0,
				new ModelResourceLocation(item.getRegistryName().toString(), "inventory"));
	}

}
