package io.github.danielm59.oreplants.block.crop;

import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockCropNether extends BlockCropBase {

	public BlockCropNether() {
		super();
		this.setRegistryName(Reference.MODID, "nethercrop");
		this.setUnlocalizedName(this.getRegistryName().toString());
		GameRegistry.register(this);
	}

	@Override
	protected Item getSeed() {

		return Registry.NetherSeeds;
	}

	@Override
	protected Item getCrop() {

		return Registry.NetherShard;
	}

}
