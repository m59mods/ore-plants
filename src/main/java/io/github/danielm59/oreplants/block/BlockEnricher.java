package io.github.danielm59.oreplants.block;

import io.github.danielm59.oreplants.OrePlants;
import io.github.danielm59.oreplants.reference.GuiId;
import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.tile.TileEnricher;
import io.github.danielm59.oreplants.tile.TileInfuser;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockEnricher extends BlockMachine{

	public BlockEnricher(){
		super();
		this.setRegistryName(new ResourceLocation(Reference.MODID, "enricher"));
		this.setUnlocalizedName(this.getRegistryName().toString());
		item = new ItemBlock(this);
		item.setRegistryName(this.getRegistryName());
		item.setUnlocalizedName(this.getUnlocalizedName());
		GameRegistry.register(this);
		GameRegistry.register(item);
	}
	
	@Override
	public boolean onBlockActivated(World world, BlockPos p, IBlockState state, EntityPlayer player, EnumHand hand,
			ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ) {

		if (player.isSneaking()) {
			return true;
		} else {
			if (!world.isRemote && world.getTileEntity(p) instanceof TileEnricher) {
				player.openGui(OrePlants.instance, GuiId.ENRICHER.ordinal(), world, p.getX(), p.getY(), p.getZ());
			}
			return true;
		}
	}
	
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEnricher();
	}

}
