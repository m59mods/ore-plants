package io.github.danielm59.oreplants.block.crop;

import io.github.danielm59.oreplants.reference.Reference;
import io.github.danielm59.oreplants.registry.Registry;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockCropDiamond extends BlockCropBase {

	public BlockCropDiamond() {
		super();
		this.setRegistryName(Reference.MODID, "diamondcrop");
		this.setUnlocalizedName(this.getRegistryName().toString());
		GameRegistry.register(this);
	}

	@Override
	protected Item getSeed() {

		return Registry.DiamondSeeds;
	}

	@Override
	protected Item getCrop() {

		return Registry.DiamondShard;
	}

}
