package io.github.danielm59.oreplants.block;

import io.github.danielm59.oreplants.reference.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockEssence extends Block {

	private final Item item;
	
	public BlockEssence() {
		super(Material.ROCK);
		this.setSoundType(SoundType.STONE);
		this.setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
		this.setHardness(2.5F);
		this.setResistance(10.0F);
		this.setRegistryName(new ResourceLocation(Reference.MODID, "essenceblock"));
		this.setUnlocalizedName(this.getRegistryName().toString());
		item = new ItemBlock(this);
		item.setRegistryName(this.getRegistryName());
		item.setUnlocalizedName(this.getUnlocalizedName());
		GameRegistry.register(this);
		GameRegistry.register(item);
	}
	
	public void registerTexture(){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0,
				new ModelResourceLocation(item.getRegistryName().toString(), "inventory"));
	}

}
