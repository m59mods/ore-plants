package io.github.danielm59.oreplants.recipe;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;

public class EnricherRegistry {

	private static EnricherRegistry INSTANCE = new EnricherRegistry();
	private final static List<EnricherRecipe> enricherRecipes = new ArrayList<EnricherRecipe>();

	private EnricherRegistry() {

	}

	public static EnricherRegistry getInstance() {

		return INSTANCE;
	}

	public static void addRecipe(EnricherRecipe recipe) {

		enricherRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output) {

		addRecipe(new EnricherRecipe(inputTop, inputBottom, output));

	}

	public static List<EnricherRecipe> getAllRecipes() {

		return enricherRecipes;
	}

	public EnricherRecipe getMatchingRecipe(ItemStack inputTopSlot, ItemStack inputBottomSlot, ItemStack outputSlot) {

		for (EnricherRecipe recipe : enricherRecipes) {
			if (inputTopSlot != null) {
				if (recipe.getInputTop().isItemEqual(inputTopSlot)) {
					if (inputBottomSlot != null) {
						if (recipe.getInputBottom().isItemEqual(inputBottomSlot)) {
							if (outputSlot != null) {
								ItemStack craftingResult = recipe.getOutput();
								if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult)
										|| !outputSlot.isItemEqual(craftingResult)) {
									continue;
								} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot
										.getMaxStackSize()) {
									continue;
								}
							}
							return recipe;
						}
					}
				}
			}
		}
		return null;
	}
}
