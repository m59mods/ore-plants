package io.github.danielm59.oreplants.recipe;

import net.minecraft.item.ItemStack;

public class EnricherRecipe {
	
	private ItemStack inputTop;
	private ItemStack inputBottom;
	private ItemStack output;

	EnricherRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output)
	{

		this.inputTop = inputTop;
		this.inputBottom = inputBottom;
		this.output = output;

	}

	public ItemStack getInputTop()
	{

		return inputTop;

	}

	public ItemStack getInputBottom()
	{

		return inputBottom;

	}

	public ItemStack getOutput()
	{

		return output;

	}	
}
