package io.github.danielm59.oreplants.handler;

import io.github.danielm59.oreplants.gui.GuiEnricher;
import io.github.danielm59.oreplants.gui.GuiInfuser;
import io.github.danielm59.oreplants.inventory.ContainerEnricher;
import io.github.danielm59.oreplants.inventory.ContainerInfuser;
import io.github.danielm59.oreplants.reference.GuiId;
import io.github.danielm59.oreplants.tile.TileEnricher;
import io.github.danielm59.oreplants.tile.TileInfuser;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
	@Override
	public Object getServerGuiElement(int id, EntityPlayer entityPlayer, World world, int x, int y, int z) {
		if (id == GuiId.INFUSER.ordinal()) {
			TileInfuser tileInfuser = (TileInfuser) world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerInfuser(entityPlayer.inventory, tileInfuser, entityPlayer);
		}
		if (id == GuiId.ENRICHER.ordinal()) {
			TileEnricher tileEnricher = (TileEnricher) world.getTileEntity(new BlockPos(x, y, z));
			return new ContainerEnricher(entityPlayer.inventory, tileEnricher, entityPlayer);
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer entityPlayer, World world, int x, int y, int z) {
		if (id == GuiId.INFUSER.ordinal()) {
			TileInfuser tileInfuser = (TileInfuser) world.getTileEntity(new BlockPos(x, y, z));
			return new GuiInfuser(entityPlayer.inventory, tileInfuser, entityPlayer);
		}
		if (id == GuiId.ENRICHER.ordinal()) {
			TileEnricher tileEnricher = (TileEnricher) world.getTileEntity(new BlockPos(x, y, z));
			return new GuiEnricher(entityPlayer.inventory, tileEnricher, entityPlayer);
		}
		return null;
	}
}
